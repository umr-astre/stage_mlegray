
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Stage Maxime Legray

<!-- badges: start -->
<!-- badges: end -->

Context: EuFMD. Development of a R-package for the risk of introduction.

Comparison of scaling procedures for the assessment of the risk of
introduction.

See the [full
report](https://umr-astre.pages.mia.inra.fr/stage_mlegray/scaling_methods.html)

![Final introduction risk maps using current (left) and proposed (right)
scaling functions.](README_files/figure-gfm/risk-intro-map-1.png)
