---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# Stage Maxime Legray

<!-- badges: start -->
<!-- badges: end -->

Context: EuFMD. Development of a R-package for the risk of introduction. 

Comparison of scaling procedures for the assessment of the risk of introduction.

See the [full report](https://umr-astre.pages.mia.inra.fr/stage_mlegray/scaling_methods.html)


```{r risk-intro-map, fig.cap = cap, echo = FALSE}
cap <- "Final introduction risk maps using current (left) and proposed (right) scaling functions."
library(targets)
library(ggplot2)
library(patchwork)
tar_load(risks_intro)

p0 <- ggplot(risks_intro) +
  geom_sf(aes(fill = ordered(risk_max_0))) +
  scale_fill_viridis_d("Risk intro", guide = guide_legend(reverse = TRUE)) 

p1 <- ggplot(risks_intro) +
  geom_sf(aes(fill = risk_max)) +
  scale_fill_viridis_c("Risk intro")

p0 + p1
```

